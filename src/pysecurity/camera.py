import io
import math
import json
from pathlib import Path
from datetime import datetime
from threading import Thread, Condition

import PIL
import imageio as iio
from PIL import Image, ImageChops
from picamera import PiCamera, PiCameraCircularIO

from pysecurity.app import get_config

from typing import Union


class PySecurityStream(PiCameraCircularIO):
    def __init__(self):
        self.new_frame = None
        self.__buffer = io.BytesIO()
        self.condition = Condition()
        self.__frames = []
        self.num_seconds = 3
        self.diff_seconds = 3
        self.entropies = []

    def setup_params(
        self,
        app,
        framerate: int,
        seconds: int,
        *args, **kwargs,
    ):
        super().__init__(*args, seconds=1, **kwargs)
        self.app = app
        self.framerate = framerate
        self.num_seconds = seconds

    def has_motion(self) -> bool:
        entropies = [*self.entropies]
        entropies.sort()
        num_low_entropies = min(self.framerate*self.diff_seconds, len(entropies))
        baseline_entropy = sum(entropies[:num_low_entropies]) / num_low_entropies
        sensitivity = float(get_config(self.app, "sensitivity"))
        trigger_value = baseline_entropy * (1. + (sensitivity / 100))
        current_entropy = self.entropies[-1]

        print(f"{baseline_entropy = }")
        print(f"{trigger_value = }")
        print(f"{current_entropy = }")
        return trigger_value < current_entropy

    def detect_motion(self) -> None:
        if len(self.__frames) < self.framerate * self.diff_seconds: return

        try:
            oldest = Image.open(io.BytesIO(self.__frames[-(self.framerate*self.diff_seconds)]))
            recent = Image.open(io.BytesIO(self.__frames[-1]))
            res_x, res_y = get_config(self.app, "resolution").split('x')
            res_x, res_y = int(res_x), int(res_y)
            black = Image.new(mode="RGBA", size=(res_x, res_y))
            mask = Image.new(mode="1", size=(res_x, res_y), color=1)
            mask_pixels = mask.load()
            zones = json.loads(get_config(self.app, "zones"))
            if len(zones) <= 0: mask = Image.new(mode="1", size=(res_x, res_y), color=0)
            for zone in zones:
                start_x = int(res_x*zone["x"]/100.)
                end_x = min(start_x + int(res_x*zone["width"]/100.), res_x-1)
                start_y = int(res_y*zone["y"]/100.)
                end_y = min(start_y + int(res_y*zone["height"]/100.), res_y-1)
                for x in range(start_x, end_x):
                    for y in range(start_y, end_y):
                        mask_pixels[x,y] = 0
            oldest = Image.composite(black, oldest, mask)
            recent = Image.composite(black, recent, mask)

            diff = ImageChops.difference(oldest, recent)
            histogram = diff.histogram()
            histogram_length = sum(histogram)
            samples_probability = [float(h) / histogram_length for h in histogram]
            entropy = -sum([p * math.log(p, 2) for p in samples_probability if p != 0])

            self.entropies.append(entropy)
            while len(self.entropies) > self.framerate*self.diff_seconds*2:
                self.entropies.pop(0)
        except PIL.UnidentifiedImageError:
            pass

    def save_thumbnail(self, path: Union[str, Path]) -> None:
        frame = self.__frames[-1]
        image = Image.open(io.BytesIO(frame))
        image.save(path)

    def save_video(self, path: Union[str, Path], seconds: int=10) -> None:
        max_frames = min(len(self.__frames) - 1, seconds * self.framerate)
        frames = self.__frames[-max_frames: -1]
        writer = iio.get_writer(path, fps=self.framerate)
        for frame in frames:
            image = Image.open(io.BytesIO(frame))
            img_bytes = io.BytesIO()
            image.save(img_bytes, format='PNG')
            writer.append_data(iio.imread(img_bytes.getvalue()))
        writer.close()

    def write(self, b: bytes) -> int:
        n = super().write(b)

        if b.startswith(b"\xff\xd8"):
            self.__buffer.truncate()
            with self.condition:
                self.new_frame = self.__buffer.getvalue()
                self.__frames.append(self.new_frame)
                extra_buffer_seconds = 5   # just in case writing takes longer than this, keep some extra frames
                if len(self.__frames) > self.framerate*(self.num_seconds+extra_buffer_seconds):
                    self.__frames.pop(0)
                self.detect_motion()
                self.condition.notify_all()
            self.__buffer.seek(0)
        self.__buffer.write(b)
            
        return n

stream = PySecurityStream()


class PySecurityCamera(Thread):

    def __init__(
        self,
        basepath: Path,
        app,
        seconds: int=60,
        resolution: str="480x480",
        fps: int=12,
        *args, **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.basepath = basepath
        self.app = app
        self.seconds = seconds
        self.resolution = resolution
        self.fps = fps

    def run(self):
        camera = PiCamera(
            resolution=self.resolution,
            framerate=self.fps,
        )
        stream.setup_params(
            self.app,
            self.fps,
            self.seconds,
            camera,
        )
        camera.start_recording(stream, format="mjpeg")
        clips_dir = self.basepath / "clips"
        clips_dir.mkdir(parents=True, exist_ok=True)
        try:
            camera.wait_recording(2*stream.diff_seconds)
            while True:
                camera.wait_recording(1)
                if stream.has_motion():
                    print("motion detected")
                    filename = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                    stream.save_thumbnail(clips_dir / f"{filename}.jpeg")
                    camera.wait_recording(self.seconds/2)
                    stream.save_video(clips_dir / f"{filename}.mp4", seconds=self.seconds)
        finally:
            print("shutting down")
            camera.stop_recording()
                        
