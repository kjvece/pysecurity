import os
import glob
from pathlib import Path

from flask import Blueprint, current_app, jsonify, send_from_directory

clips = Blueprint("clips", __name__)

@clips.route("/clips")
def handle_clips():
    clips_dir = current_app.storage_path / "clips"
    clips = list(set(str(Path(clip).with_suffix("")) for clip in os.listdir(clips_dir)))
    clips.sort()
    clips = list(reversed(clips))

    return jsonify(clips)

@clips.route("/clips/<path:clip>")
def handle_clip(clip):
    clips_dir = current_app.storage_path / "clips"
    return send_from_directory(clips_dir, clip)

@clips.route("/clips/delete/<path:clip>")
def handle_delete(clip):
    clips_dir = current_app.storage_path / "clips"
    files = glob.glob(str(clips_dir / f"{clip}*"))
    print(f"deleting: {files}")
    for file in files:
        os.unlink(file)
    return jsonify(files)
