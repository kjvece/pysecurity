import json

from flask import Blueprint, jsonify, request, Response, current_app

from pysecurity.app import get_config, write_config

zones = Blueprint("zones", __name__)

@zones.route("/zones", methods=["GET"])
def handle_zones():
    zones = json.loads(get_config(current_app, "zones"))
    return jsonify(zones)

@zones.route("/zones", methods=["POST"])
def handle_post_zones():
    data = request.get_json(force=True)
    try:
        zones = json.dumps(data).replace("%", "%%")
    except:
        zones = "[]"
    write_config(current_app, "zones", zones)
    return Response(status=200)
