from picamera import PiCamera
from flask import Blueprint, Response

from pysecurity.camera import stream

def gen():
    while True:
        with stream.condition:
            stream.condition.wait()
            frame = stream.new_frame
        yield (
            b"--frame\r\n"
            b"Content-Type: image/jpeg\r\n\r\n"
            + frame
            + b"\r\n"
        )

camera = Blueprint("camera", __name__)

@camera.route("/stream.mjpg")
def handle_stream():
    return Response(gen(), mimetype="multipart/x-mixed-replace; boundary=frame")
