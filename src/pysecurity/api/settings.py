from flask import Blueprint, current_app, jsonify, request, abort, Response

from pysecurity.app import get_config, write_config, DEFAULT_CONFIG

settings = Blueprint("settings", __name__)


@settings.route("/settings", methods=["GET"])
def handle_get_settings():
    default_config = {**DEFAULT_CONFIG}
    for key in default_config:
        default_config[key] = get_config(current_app, key)
    return jsonify(default_config)


@settings.route("/settings", methods=["POST"])
def handle_post_settings():
    data = request.get_json(force=True)
    key = data.get("key")
    val = data.get("val")
    if key is None or val is None: abort(400, description="Bad request")
    write_config(current_app, key, str(val))
    return Response(status=200)


@settings.route("/settings/default", methods=["POST"])
def handle_default_settings():
    data = request.get_json(force=True)
    key = data.get("key")
    if key is None: abort(400, description="Bad request")
    val = get_config(current_app, key, default=True)
    return jsonify({ "val": val })
