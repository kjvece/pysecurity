import logging
import argparse
import waitress
import configparser
from pathlib import Path

from .app import create_app, get_config
from .camera import PySecurityCamera

def register(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.set_defaults(func=serve)
    parser.add_argument(
        "-i",
        "--interface",
        action="store",
        type=str,
        default="0.0.0.0",
        help="address to bind to (default 0.0.0.0)",
    )
    parser.add_argument(
        "-p",
        "--port",
        action="store",
        type=int,
        default=8000,
        help="port to serve on (default 8000)",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="enable debugging",
    )
    parser.add_argument(
        "--data",
        action="store",
        type=Path,
        default=Path.home() / ".local" / "share" / "pysecurity",
        help="path to store pysecurity data",
    )
    return parser

def serve(args: argparse.Namespace) -> None:
    storage = args.data
    storage.mkdir(exist_ok=True, parents=True)

    app = create_app(storage)

    seconds = int(get_config(app, "seconds"))
    resolution = get_config(app, "resolution")
    fps = int(get_config(app, "fps"))

    PySecurityCamera(
        basepath=app.storage_path,
        app=app,
        seconds=seconds,
        resolution=resolution,
        fps=fps,
        daemon=True,
    ).start()

    logger = logging.getLogger("waitress")
    logger.setLevel(logging.DEBUG)

    waitress.serve(
        app,
        listen=f"{args.interface}:{args.port}",
    )
