#!/usr/bin/env python

import argparse

import pysecurity

def main() -> None:
    parser = argparse.ArgumentParser()

    # register subparsers here
    pysecurity.serve.register(parser)

    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
