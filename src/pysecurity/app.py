import os
import shutil
import configparser
import importlib.resources
from pathlib import Path

from flask import Flask, current_app


from typing import Optional


DEFAULT_CONFIG = {
    "seconds": "20",    # length of clip that will be recorded
    "resolution": "480x480",
    "fps": "6",
    "sensitivity": "20.0",
    "zones": "[]",
}


def get_config(app: Flask, key: str, default: bool=False) -> Optional[str]:
    if default: return DEFAULT_CONFIG.get(key)

    value = app.config_file["DEFAULT"].get(key)
    if value is None:
        return DEFAULT_CONFIG.get(key)
    return value


def write_config(app: Flask, key: str, val: str) -> None:
    app.config_file["DEFAULT"][key] = val
    with open(app.config_path, "w", encoding="utf8") as config_file:
        app.config_file.write(config_file)
    

def create_app(storage: Path) -> Flask:

    config = configparser.ConfigParser()
    config_path = storage / "config.ini"
    config.read(config_path)
    
    static_path = storage / "static"
    model_path = storage / "model"

    for data in importlib.resources.files("pysecurity").iterdir():
        if data.is_dir() and os.path.basename(data) == "data":
            shutil.copytree(data, static_path, dirs_exist_ok=True)
        """ TODO: copy model when it's working
        if data.is_dir() and os.path.basename(data) == "model":
            shutil.copytree(data, model_path, dirs_exist_ok=True)
        """

    app = Flask(
        __name__,
        static_url_path="",
        static_folder=static_path,
    )
    app.config_file = config
    app.config_path = config_path
    app.storage_path = storage
    app.model_path = model_path / "human_detector.pt"

    @app.route("/")
    def index():
        return current_app.send_static_file("index.html")

    from pysecurity.api.camera import camera
    from pysecurity.api.clips import clips
    from pysecurity.api.settings import settings
    from pysecurity.api.zones import zones
    app.register_blueprint(camera)
    app.register_blueprint(clips)
    app.register_blueprint(settings)
    app.register_blueprint(zones)

    return app
