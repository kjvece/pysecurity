#!/usr/bin/env python

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as T

from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader, random_split

from ignite.engine import Events, create_supervised_trainer, create_supervised_evaluator
from ignite.metrics import Accuracy, Loss
from ignite.handlers import EarlyStopping

if __name__ == "__main__":
    data = ImageFolder("./human-detection-dataset/human detection dataset", transform=T.Compose([
        T.Resize((224,224)),
        T.RandomAffine(20),
        T.RandomHorizontalFlip(),
        T.RandomAutocontrast(.2),
        T.RandomInvert(.2),
        T.ColorJitter(),
        T.ToTensor(),
    ]))
    print(f"Number of samples: {len(data)}")
    train_size = int(len(data)*.8)
    val_size = len(data) - train_size
    train_data, val_data = random_split(data, [train_size, val_size])
    print(f"Training size: {len(train_data)}")
    print(f"Validation size: {len(val_data)}")


    batch_size = 32
    lr = .0001
    log_interval = 1
    num_workers = 8

    train_loader = DataLoader(
        train_data,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
    )
    val_loader = DataLoader(
        val_data,
        batch_size=batch_size,
        shuffle=False,
        num_workers=num_workers,
    )

    train_x, train_y = next(iter(train_loader))
    print(f"Size of batch: {train_x.shape}")
    print(f"Output shape: {train_y.shape}")

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_efficientnet_b4', pretrained=True)
    model.classifier.fc = nn.Sequential(
        nn.Linear(1792, 1),
        nn.Flatten(0),
    )
    model = model.to(device)

    optimizer = optim.Adam(model.parameters(), lr=lr)
    bceloss = nn.BCEWithLogitsLoss()
    def BCEWithLogitsAndFloatsLoss(y_pred, y):
        return bceloss(y_pred.float(), y.float())
    criterion = BCEWithLogitsAndFloatsLoss

    def output_transform(output):
        y_pred, y = output
        y_pred = y_pred > 0.5
        y = y > 0.4
        return y_pred.long(), y.long()

    trainer = create_supervised_trainer(model, optimizer, criterion, device=device)
    val_metrics = {
        "loss": Loss(criterion),
        "accuracy": Accuracy(output_transform=output_transform),
    }
    evaluator = create_supervised_evaluator(model, metrics=val_metrics, device=device)

    @trainer.on(Events.EPOCH_COMPLETED)
    def log_training_results(trainer):
        evaluator.run(train_loader)
        metrics = evaluator.state.metrics
        print(f"Training - Epoch: {trainer.state.epoch}  Avg accuracy: {metrics['accuracy']:.3f} Avg loss: {metrics['loss']:.6f}")

    @trainer.on(Events.EPOCH_COMPLETED)
    def log_validation_results(trainer):
        evaluator.run(val_loader)
        metrics = evaluator.state.metrics
        print(f"Validation - Epoch: {trainer.state.epoch}  Avg accuracy: {metrics['accuracy']:.3f} Avg loss: {metrics['loss']:.6f}")

    def score_function(engine):
        val_loss = engine.state.metrics['loss']
        return -val_loss

    handler = EarlyStopping(patience=3, score_function=score_function, trainer=trainer)
    evaluator.add_event_handler(Events.COMPLETED, handler)

    trainer.run(train_loader, max_epochs=100)

    model = model.cpu()
    model_scripted = torch.jit.script(model)
    model_scripted.save("../pysecurity/model/human_detector.pt")
