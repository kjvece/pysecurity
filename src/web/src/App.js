import React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Copyright from './components/Copyright';
import PySecurityAppBar from './components/PySecurityAppBar';
import { Outlet } from 'react-router-dom';

export default function App() {
  return (
    <>
      <PySecurityAppBar />
      <Container maxWidth="sm">
        <Box sx={{ my: 4 }}>
          <Outlet />
          <Copyright />
        </Box>
      </Container>
    </>
  );
}
