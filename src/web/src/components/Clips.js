import React, { useState, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import DeleteForever from '@mui/icons-material/DeleteForever';
import ReactPlayer from 'react-player/lazy';

const modalStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: '70%',
  maxWidth: '90%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const Clips = () => {

    const [clips, setClips] = useState([]);

    useEffect(() => {
        const updateClips = async () => {
            fetch('/clips')
                .then(response => response.json())
                .then(data => setClips(data))
        }
        setTimeout(updateClips, 0);

        const interval = setInterval(updateClips, 500);
        return () => clearInterval(interval);
    }, []);
    
    const [open, setOpen] = useState(false);
    const [url, setUrl] = useState(null);

    function handleOpen(clip) {
        return () => {
            setUrl(clip);
            setOpen(true);
        }
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <>
            <Typography variant='h3'>Saved Clips</Typography>
            <Modal
                open={open}
                onClose={handleClose}
            >
                <Box sx={modalStyle}>
                    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
                        <Typography variant="h5">{url}</Typography>
                        <Box sx={{ maxHeight: 'max-content', maxWidth: 'max-content' }}>
                            <ReactPlayer
                                url={`/clips/${url}.mp4`}
                                controls={true}
                                loop={true}
                                playing={true}
                                width='100%'
                                height='100%'
                            />
                        </Box>
                    </Box>
                </Box>
            </Modal>
            {clips.map(clip => {
                return (
                    <React.Fragment key={clip}>
                        <Box sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            paddingTop: '2em',
                            paddingBottom: '1em',
                        }}>
                            <Typography variant='h6'>{clip}</Typography>
                            <Button variant='outlined' onClick={() => fetch(`/clips/delete/${clip}`)}><DeleteForever /></Button>
                        </Box>
                        <Button sx={{ width: '100%' }} >
                            <Box
                                component='img'
                                src={`/clips/${clip}.jpeg`}
                                sx={{ width: '100%', height: '100%' }}
                                alt='thumbnail'
                                onClick={handleOpen(clip)}
                            />
                        </Button>
                    </React.Fragment>
                )
            })}
        </>
    );
}

export default Clips;
