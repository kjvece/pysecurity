import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Stream from './Stream';

const LiveView = () => {
    return <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Typography variant='h4' sx={{ paddingBottom: '.5em' }} >Live View</Typography>
        <Stream />
    </Box>
}

export default LiveView;
