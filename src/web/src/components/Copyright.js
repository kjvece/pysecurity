import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

export default function Copyright() {
  return (
    <Typography variant="body2" sx={{ paddingTop: '1em' }} color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://gitlab.com/kjvece/pysecurity">
        PySecurity
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

