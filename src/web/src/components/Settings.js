import React, { useState, useEffect, cloneElement } from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import Slider from '@mui/material/Slider';
import Button from '@mui/material/Button';

import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import Stream from './Stream';

const Setting = ({
    id,
    label,
    field,
    currValue,
    defaultValue,
    setChanged,
}) => {
    const [current, setCurrent] = useState(currValue);

    useEffect(() => {
        setCurrent(currValue);
    }, [setCurrent, currValue]);

    const preppedField = cloneElement(field, {
        onChange: (event) => {
            setCurrent(event.target.value);
        },
        value: typeof current === 'number' && !isNaN(current) ? current : defaultValue,
    });

    const handleSave = () => {
        fetch('/settings', {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                key: id,
                val: current,
            }),
        }).then(() => {
            setChanged({});
        });
    };

    const handleReset = () => {
        setCurrent(currValue);
    };

    const handleDefault = () => {
        setCurrent(defaultValue);
    };

    return <Box sx={{ paddingBottom: '1em' }} >
        <InputLabel htmlFor={id}>{label}</InputLabel>
        {preppedField}
        <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button sx={{ margin: '.2em', width: '6em' }} variant='contained' onClick={handleSave}>
                Save
            </Button>
            <Button sx={{ margin: '.2em', width: '6em' }} variant='contained' onClick={handleReset}>
                Reset
            </Button>
            <Button sx={{ margin: '.2em', width: '6em' }} variant='contained' onClick={handleDefault}>
                Default
            </Button>
        </Box>
    </Box>
}

const SetZone = ({
    setChanged,
    setActive,
    currentZones,
}) => {
    const [zone, setZone] = useState(currentZones);

    useEffect(() => {
        // TODO: update to use multiple zones
        const zone = currentZones !== undefined ? currentZones[0] : null;
        setZone(zone);
    }, [setZone, currentZones]);

    const handleApply = () => {
        setActive(false);
        let z = [zone];
        if (zone === undefined || zone.width === 0 || zone.height === 0) z = [];
        console.log('applying', z);
        fetch('/zones', {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify(z),
        }).then(() => {
            setChanged({});
        });
    }

    const handleCancel = () => {
        setActive(false);
    }

    return <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
        <Box sx={{ display: 'flex' }} >
            <Button onClick={handleApply}>Apply</Button>
            <Button onClick={handleCancel}>Cancel</Button>
        </Box>
        <ReactCrop crop={zone} onChange={(_,p) => {setZone(p)}}>
            <Stream />
        </ReactCrop>
    </Box>
}

const DisplayZone = ({
    setActive,
    zones,
}) => {
    // TODO: update to use multiple zones
    const zone = zones !== undefined ? zones[0] : null;
    return <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
        <Button onClick={() => setActive(p => !p)}>Edit</Button>
        <ReactCrop crop={zone} disabled>
            <Stream />
        </ReactCrop>
    </Box>
}

const ZoneManager = ({
        setChanged,
        currentZones,
    }) => {
    const [active, setActive] = useState(false);
    const [zones, setZones] = useState([]);

    useEffect(() => {
        setZones(currentZones);
    }, [setZones, currentZones]);

    return <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
        <Typography variant='h5' sx={{ paddingBottom: '.5em' }}>Zones</Typography>
        {!active && <DisplayZone setActive={setActive} zones={zones}/>}
        {active && <SetZone setChanged={setChanged} setActive={setActive} currentZones={zones} />}
    </Box>
}

const Settings = () => {

    const [settings, setSettings] = useState({});
    const [changed, setChanged] = useState({});

    useEffect(() => {
        fetch('/settings')
            .then(response => response.json())
            .then(data => setSettings(data))
    }, [
        changed,
        setSettings,
    ]);

    const fps = parseInt(settings['fps']);
    const seconds = parseInt(settings['seconds']);
    const sensitivity = parseFloat(settings['sensitivity']);
    const zones = settings['zones'] !== undefined ? JSON.parse(settings['zones']) : [];

    return <Box>
        <Typography variant='h4'>Settings</Typography>
        <Setting
            id='sensitivity'
            label='Sensitivity (higher is LESS sensitive)'
            field={<Slider
                id='sensitivity'
                step={.1}
                min={1}
                max={50}
                valueLabelDisplay='on'
                sx={{ paddingTop: '6em', marginBottom: '-2em' }}
            />}
            currValue={sensitivity}
            defaultValue={10.0}
            setChanged={setChanged}
        />
        <Setting
            id='seconds'
            label='Seconds per clip (requires reboot)'
            field={<Slider
                id='fps'
                step={1}
                marks
                min={10}
                max={300}
                valueLabelDisplay='on'
                sx={{ paddingTop: '6em', marginBottom: '-2em' }}
            />}
            currValue={seconds}
            defaultValue={30}
            setChanged={setChanged}
        />
        <Setting
            id='fps'
            label='Framerate (requires reboot)'
            field={<Slider
                id='fps'
                step={1}
                marks
                min={1}
                max={12}
                valueLabelDisplay='on'
                sx={{ paddingTop: '6em', marginBottom: '-2em' }}
            />}
            currValue={fps}
            defaultValue={6}
            setChanged={setChanged}
        />
        <ZoneManager currentZones={zones} setChanged={setChanged} />
    </Box>
}

export default Settings;
