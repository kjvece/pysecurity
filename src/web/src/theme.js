import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      light: '#2c387e',
      main: '#3f51b5',
      dark: '#6573c3',
    },
    secondary: {
      light: '#52b202',
      main: '#76ff03',
      dark: '#91ff35',
    },
    mode: 'dark',
  },
});

export default theme;
