import React from 'react';
import ReactDOM from 'react-dom/client';
import {
    BrowserRouter,
    Routes,
    Route,
} from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';

import App from './App';
import LiveView from './components/LiveView';
import Clips from './components/Clips';
import Settings from './components/Settings';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
     <CssBaseline />
     <BrowserRouter>
      <Routes>
       <Route path="/" element={<App />}>
         <Route index element={<LiveView />} />
         <Route path="saved" element={<Clips />} />
         <Route path="settings" element={<Settings />} />
       </Route>
      </Routes>
     </BrowserRouter>
    </ThemeProvider>
  </React.StrictMode>
);
