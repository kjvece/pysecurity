# PySecurity

An open source IoT smart camera implementation for Raspberry Pi

## Features
* Local storage and streaming
* Motion detection and saved clips
* Motion zones and editor
* Human detection (incomplete, model is built but I need 32-bit pytorch to run it if someone wants to help me build it)

###### Planned Development
* HTTPS support
* Multiple camera support
* User login/security
* Cloud push notifications/viewer

## Requirements
* Raspberry pi with 32-bit OS (64-bit not supported currently)
* Picamera

## Installation

Easiest installation is with pip:
```
pip install git+https://gitlab.com/kjvece/pysecurity.git
```

Upon installation, the `pysecurity` command is added to the user path.

## Usage

Easiest way to run the program is simply:
```
pysecurity
```

By default, the web interface is exposed on port 8000, this can be changed with the `-p`/`--port` option
```
pysecurity -p 80
```

The full help can be viewed with the `-h`/`--help` flag and is shown below:
```
usage: pysecurity [-h] [-i INTERFACE] [-p PORT] [-d] [--data DATA]

optional arguments:
  -h, --help            show this help message and exit
  -i INTERFACE, --interface INTERFACE
                        address to bind to (default 0.0.0.0)
  -p PORT, --port PORT  port to serve on (default 8000)
  -d, --debug           enable debugging
  --data DATA           path to store pysecurity data
```
