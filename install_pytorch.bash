#!/bin/bash

sudo apt update
sudo apt install clang

sudo apt install ninja-build git cmake ninja-build git cmake libopenblas-dev libblas-dev libeigen3-dev

pip install wheel mock pillow

pip install setuptools==58.3.0

git clone -b v1.8.0 --depth=1 --recursive https://github.com/pytorch/pytorch.git

cd pytorch
pip install -r requirements.txt

export BUILD_CAFFE2_OPS=OFF
export USE_FBGEMM=OFF
export USE_FAKELOWP=OFF
export BUILD_TEST=OFF
export USE_MKLDNN=OFF
export USE_NNPACK=ON
export USE_XNNPACK=ON
export USE_QNNPACK=ON
export MAX_JOBS=4
export USE_OPENCV=OFF
export USE_NCCL=OFF
export USE_SYSTEM_NCCL=OFF
PATH=/usr/lib/ccache:$PATH
export CC=clang
export CXX=clang++

python3 setup.py clean

python3 setup.py bdist_wheel

cd dist
pip install ./torch*.whl
